import scala.sys.process._
import scala.io.Source
import java.io.PrintWriter


/**
* ILGDBのデータセットを一つのフォルダにまとめる & 速度データに直す。
*/


  val base_folder = args.toList.head //


  val datasets = Process("ls " + base_folder).lines.toList.flatMap(x => {
    Process("ls " + base_folder + "/" + x).lines.toList.flatMap(y => {
      Process("ls " + base_folder + "/" + x + "/" + y).lines.toList.map(z => {
        base_folder + "/" + x + "/" + y + "/" + z
      })
    })
  })

  val output_folder = base_folder + "/../flat"
  val ofn = output_folder + "/%s"
  /**
   * 
   */
  def saveToFileAsV(path: String, head : String, l: List[String]) = {
    val ll = l.reverse
    println(path)
    val out = new PrintWriter(path)
    out.println(head) //ファイルヘッダの書き込み
    
    out.println("0,0")

    var (prev_x : Int, prev_y : Int) = {
        val t = ll.head.split(" ")
    	(t(0).toInt,t(1).toInt)
    }
    
    ll.tail.foreach(q => {
    	val (x,y) = {
    	  val t = q.split(" ")
    	  (t(0).toInt,t(1).toInt)
    	}
    	out.println( (x - prev_x) + "," + (y - prev_y))
    	prev_x = x
    	prev_y = y
     }
    )
    out.close
  }
  var i : Int = 0
  def flatFormat(base_file_name: String, ll: List[String], fl: List[String] = Nil, file_header: String = null): Unit = {
    def formatPathName() = {
     i = i + 1
      ofn.format(file_header.split(" ").head.drop(1) + "_" + i + "_" + base_file_name)
    }

    ll match {
      case Nil => saveToFileAsV(formatPathName, file_header, fl)
      case x if x == "" => flatFormat(base_file_name, ll.tail, fl, file_header) //空白読み飛ばし
      case x if x.head.contains("#") => {
        if (file_header == null) { //first
          flatFormat(base_file_name, ll.tail, Nil, x.head)
        } else {
          saveToFileAsV(formatPathName, file_header, fl)
          flatFormat(base_file_name, ll.tail, Nil, x.head)
        }
      }
      case _ => {
        flatFormat(base_file_name, ll.tail, ll.head :: fl, file_header)
      }
    }
  }

  datasets.foreach(x => {
    val fn = x.split("/").last
    val l = Source.fromFile(x).getLines.toList
    flatFormat(fn, l)
  })
}