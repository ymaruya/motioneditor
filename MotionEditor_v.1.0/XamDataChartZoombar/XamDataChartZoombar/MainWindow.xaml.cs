﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Infragistics.Controls;
using Microsoft.Win32;
using System.IO;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using System.Windows.Threading;
using System.Globalization;
using Microsoft.Research.DynamicDataDisplay.PointMarkers;

namespace XamDataChartZoombar
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// メソッドがたくさん。
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            setPictureUpdateTimer();
        }

       /**
       * アニメーション
       */
        double phase = 0; //何フェーズか
        int anime_status = 0; //0 => ストップ, 1 => 再生, -1 => 巻き戻し。。。 
        readonly double[] animatedX = new double[2];
        readonly double[] animatedY = new double[2] { 100, -100 }; //高さは固定
        EnumerableDataSource<double> animatedDataSource = null;
        Header chartHeader = new Header();
        TextBlock headerContents = new TextBlock();

        //タイマーを分けなきゃ
        readonly DispatcherTimer timer = new DispatcherTimer();

        readonly DispatcherTimer picture_update_timer = new DispatcherTimer();
        Header picture_handler = new Header();

        /**
         */ 
        private void AnimatedPlot_Timer(object sender, EventArgs e)
        {
            phase += anime_status * 0.1;

            if ((int)phase > label.Length - 1)
            {
                phase = label.Length - 1;
                anime_status = 0;
            }


            for (int i = 0; i < animatedX.Length; i++)
                animatedX[i] = phase;

            // Here it is - signal that data is updated
            animatedDataSource.RaiseDataChanged();
            headerContents.Text = String.Format(CultureInfo.InvariantCulture, "Phase = {0:N2}", phase);
        }

        Boolean is_drawn_canvas = false;
        private void PictureUpdateTimer(object sender, EventArgs e)
        {
            //キャンパスのアップデート
            if (is_drawn_canvas)
            {
                drawPictureOnCanvas();
            }
        }


        private void setPictureUpdateTimer()
        {
            picture_update_timer.Interval = TimeSpan.FromMilliseconds(1);
            picture_update_timer.Tick += PictureUpdateTimer;
            picture_update_timer.IsEnabled = true;
        }
        /**
         * アニメーションのグラフ
         */ 
        private void createStartingGraph()
        {
            for (int i = 0; i < animatedX.Length; i++)
            {
                animatedX[i] = phase;
            }

            animatedDataSource = new EnumerableDataSource<double>(animatedX);
            animatedDataSource.SetXMapping(x => x);

            EnumerableDataSource<double> ySrc = new EnumerableDataSource<double>(animatedY);
            ySrc.SetYMapping(y => y);

            // Adding graph to plotter
            plotter.AddLineGraph(new CompositeDataSource(animatedDataSource, ySrc),
                new Pen(Brushes.Magenta, 1),
                new PenDescription("phase"));

            timer.Interval = TimeSpan.FromMilliseconds(10);
            timer.Tick += AnimatedPlot_Timer;
            timer.IsEnabled = true;

            // Force evertyhing plotted to be visible
            plotter.FitToView();
        }


        /**
         * 普通のグラフ
         * x座標用
         * y座標用
         */
        LineAndMarker<ElementMarkerPointsGraph> vx_data_graph;
        LineAndMarker<ElementMarkerPointsGraph> vy_data_graph;
        private void clearDataGraph()
        {
            if (vx_data_graph != null)
            {
                vx_data_graph.LineGraph.Remove();
                vx_data_graph.MarkerGraph.Remove();
            }
            if (vy_data_graph != null)
            {
                vy_data_graph.LineGraph.Remove();
                vy_data_graph.MarkerGraph.Remove();
            }
            plotter.FitToView();
        }
        private void createDataGraph()
        {
            
            EnumerableDataSource<int> xDataSource; //1,2,3...
            EnumerableDataSource<int> vxDataSource; //x座標用
            EnumerableDataSource<int> vyDataSource; //y座標用
            IPointDataSource ds;

            int[] px = new int[vx.Length];
            for(int i = 0; i < px.Length ; i++){ //x座標用
                px[i] = i;
            }
            
            // x速度:
            vxDataSource = new EnumerableDataSource<int>(vx);
            vxDataSource.SetYMapping(Y => Y);
            vxDataSource.AddMapping(ShapeElementPointMarker.ToolTipTextProperty,
                Y => String.Format("Value is {0}", Y));

            xDataSource = new EnumerableDataSource<int>(px);
            xDataSource.SetXMapping(X => X);

            ds = new CompositeDataSource(xDataSource, vxDataSource);
            // adding graph to plotter
            vx_data_graph = plotter.AddLineGraph(ds,
                new Pen(Brushes.Aqua, 3),
                new CircleElementPointMarker
                {
                    Size = 10,
                    Brush = Brushes.Wheat,
                    Fill = Brushes.Violet
                },
                new PenDescription("x_vector"));

            //y速度 : 
            vyDataSource = new EnumerableDataSource<int>(vy);
            vyDataSource.SetYMapping(Y => Y);
            vyDataSource.AddMapping(ShapeElementPointMarker.ToolTipTextProperty,
                Y => String.Format("Value is {0}", Y));

            xDataSource = new EnumerableDataSource<int>(px);
            xDataSource.SetXMapping(X => X);

            ds = new CompositeDataSource(xDataSource, vyDataSource);
            // adding graph to plotter
            vy_data_graph = plotter.AddLineGraph(ds,
                new Pen(Brushes.Black, 3),
                new CircleElementPointMarker
                {
                    Size = 10,
                    Brush = Brushes.Wheat,
                    Fill = Brushes.Violet
                },
                new PenDescription("y_vector"));

            plotter.FitToView();
        }


        /**
         * labelグラフ
         */
        LineAndMarker<ElementMarkerPointsGraph> vx_label_graph; //グラフを消すときに使う
        LineAndMarker<ElementMarkerPointsGraph> vy_label_graph; 
        private void createLabelGraph()
        {
            EnumerableDataSource<int> xDataSource;
            EnumerableDataSource<int> yDataSource;
            IPointDataSource ds;

            List<int> _label_x = new List<int>();
            List<int> _label_vx = new List<int>();
            List<int> _label_vy = new List<int>();

            for (int i = 0; i < label.Length; i++)
            {
                if (label[i] != null)
                {
                    _label_x.Add(i);
                    _label_vx.Add(vx[i]);
                    _label_vy.Add(vy[i]);
                }

            }

            int[] label_x = _label_x.ToArray();
            int[] label_vx = _label_vx.ToArray();
            int[] label_vy = _label_vy.ToArray();

            //xv : 
            yDataSource = new EnumerableDataSource<int>(label_vx);
            yDataSource.SetYMapping(Y => Y);
            yDataSource.AddMapping(ShapeElementPointMarker.ToolTipTextProperty,
                Y => String.Format("Value is {0}", Y));

            xDataSource = new EnumerableDataSource<int>(label_x);
            xDataSource.SetXMapping(X => X);

            ds = new CompositeDataSource(xDataSource, yDataSource);
            vx_label_graph = plotter.AddLineGraph(ds,
                new Pen(Brushes.Red, 2),
                new CircleElementPointMarker
                {
                    Size = 10,
                    Brush = Brushes.Blue,
                    Fill = Brushes.Red
                },
                new PenDescription("xv label"));

            //yv : 
            yDataSource = new EnumerableDataSource<int>(label_vy);
            yDataSource.SetYMapping(Y => Y);
            yDataSource.AddMapping(ShapeElementPointMarker.ToolTipTextProperty,
                Y => String.Format("Value is {0}", Y));

            xDataSource = new EnumerableDataSource<int>(label_x);
            xDataSource.SetXMapping(X => X);

            ds = new CompositeDataSource(xDataSource, yDataSource);
            vy_label_graph = plotter.AddLineGraph(ds,
                new Pen(Brushes.Yellow, 2),
                new CircleElementPointMarker
                {
                    Size = 10,
                    Brush = Brushes.Blue,
                    Fill = Brushes.Red
                },
                new PenDescription("yv label"));


            plotter.FitToView();
        }

        private void clearLabelGraph()
        {
            if (vx_label_graph != null)
            {
                vx_label_graph.LineGraph.Remove();
                vx_label_graph.MarkerGraph.Remove();
            }
            if (vy_label_graph != null)
            {
                vy_label_graph.LineGraph.Remove();
                vy_label_graph.MarkerGraph.Remove();
            }
            plotter.FitToView();
        }

        private void updateLabelGraph()
        {
            clearLabelGraph();
            createLabelGraph();
        }

        /**
         * めんどくさいので、ラベル付けやその他は全部キーイベントでやる。
         * p -> 1~8 => ラベルをつける
         * r => ラベルを取り除く(現在のx座標から後ろのラベル or 近いラベル どっちがいいかな)
         * c => 再生の位置を設定
         * s => 生成の位置を一番初めに戻す。
         * enter => 再生
         */
        private Boolean is_p_mode = false;
        private Boolean is_simulate_start = false;
        private void KEvent(object sender, System.Windows.Input.KeyEventArgs e)
        {
            TextBlock tb = (TextBlock)cursor1.FindName("horizTextBlock"); //x座標を取得。jquery見たい。
            String text = tb.Text;
            int _x;
            if (text != "")
            {
                if ((int)double.Parse(tb.Text) < 0)
                {
                    _x = 0;
                }
                else if ((int)double.Parse(tb.Text) > label.Length - 1)
                {
                    _x = label.Length - 1;
                }
                else
                {
                    _x = (int)double.Parse(tb.Text);
                }
            }
            else
            {
                return;
            }
            if (is_p_mode)
            {
                //int _x = int.Parse(tb.Text);
                if (e.Key == Key.D1)
                {
                    label[_x] = "up";
                    updateLabelGraph();
                }
                else if (e.Key == Key.D2)
                {
                    label[_x] = "up";
                    updateLabelGraph();
                }
                else if (e.Key == Key.D3)
                {
                    label[_x] = "up";
                    updateLabelGraph();
                }
                else if (e.Key == Key.D4)
                {
                    label[_x] = "up";
                    updateLabelGraph();
                }
                else if (e.Key == Key.D5)
                {
                    label[_x] = "up";
                    updateLabelGraph();
                }
                else
                {
                    MessageBox.Show("Not exiting such a label");
                }
                is_p_mode = false;
            }
            else if (is_simulate_start)
            {
                //ここに終了時の処理
                phase = (int)phase;
                anime_status = 0;
                is_simulate_start = false;
            }
            else
            {
                if (e.Key == Key.P)
                {
                    is_p_mode = true;
                }
                else if (e.Key == Key.R) //ラベルを消去
                {
                    if (!(_x <= 0))
                    {
                        for (int i = _x; i < label.Length - 2; i++) //最初と最後のラベルは消去できない。
                        {
                            if (label[i] != null)
                            {
                                label[i] = null;
                                break;
                            }
                        }
                    }
                }
                else if (e.Key == Key.M) //次のファイルへ
                {
                    if (saveCurrentLabel() == 0)
                    {
                        file_path_buf_point += 1;
                        if (file_path_buf_point > file_path_buf.Length - 1)
                        {
                            file_path_buf_point -= 1;
                            MessageBox.Show("バッファの終端です");
                        }
                        else
                        {
                            UpdateBuffer();
                        }
                    }
                }
                else if (e.Key == Key.N) //前ののファイルへ
                {
                    if (saveCurrentLabel() == 0)
                    {
                        file_path_buf_point -= 1;
                        if (file_path_buf_point < 0)
                        {
                            file_path_buf_point += 1;
                            MessageBox.Show("バッファの先端です");
                        }
                        else
                        {
                            UpdateBuffer();
                        }
                    }
                }
                else if (e.Key == Key.C)
                {
                    phase = _x;
                }
                else if (e.Key == Key.S)
                {
                    phase = 0;
                }
                else if (e.Key == Key.Space)
                {
                    anime_status = 1;
                    //ここに開始時の処理
                    is_simulate_start = true;
                }
            }

        }
        /**
         * 
         */ 
        private int saveCurrentLabel()
        {
            string[] _t = lab_file.Split('\\');
            string o = _t[_t.Length - 1] + Environment.NewLine;
            int j = 1;
            for (int i = 1; i < label.Length; i++)
            {
                if (label[i] != null)
                {
                    if (label[i] == "e")
                    {
                        MessageBox.Show("終端ラベルが設定されてません");
                        return -1;
                    }
                    o += j + "," + (i + 1) + "," + label[i] + Environment.NewLine;
                    j = i + 1;
                }
            }
            System.IO.StreamWriter sw = new System.IO.StreamWriter(
                 lab_file,
                 false,
             System.Text.Encoding.GetEncoding("UTF-8"));
            //TextBox1.Textの内容を書き込む
            sw.Write(o);
            //閉じる
            sw.Close();
            MessageBox.Show("ラベルファイルが保存されました。");
            return 0;
        }


        string lab_file = null; //出力するラベルファイルの名前
        int[] x = null; //めんどくさいので1次元
        int[] y = null;
        int[] vx = null;
        int[] vy = null;
        string[] label = null;
        const string start_label = "s";
        const string end_label = "e";
        string[] file_path_buf;
        int file_path_buf_point = 0;

        private void initV()
        {
            is_drawn_canvas = false;
            x = null;
            y = null;
            vx = null;
            vy = null;
            label = null;
            lab_file = null;
            file_path_buf_point = 0;
            file_path_buf = null;
        }

        
        /**
         * 図を描画する。
         * プログラムで書けたほうがうれしいよね。
         */
        private Point[] mkQua(int x, int y)
        {
            Point[] xy = new Point[4];
            xy[0] = new Point(x + 1, y + 1);
            xy[1] = new Point(x + 1, y - 1);
            xy[2] = new Point(x - 1, y + 1);
            xy[3] = new Point(x - 1, y - 1);
            return xy;
        }
        private void drawPictureOnCanvas()
        {
            //binding
            // Add a Line Element
            Canvas1.Children.Clear();
            PathFigure picture_figure = new PathFigure();
           
            //convert array to point

            picture_figure.StartPoint = new Point(x[0], y[0]);
            Point[] picture_point = new Point[x.Length - 2];
            for (int i = 1; i < x.Length - 1; i++)
            {
                picture_point[i - 1] = new Point(x[i], y[i]);
            }

            PolyLineSegment picure_line_segment = new PolyLineSegment();
            picure_line_segment.Points =
                new PointCollection(picture_point);

            picture_figure.Segments.Add(picure_line_segment); 

            /// Create a PathGeometry to contain the figure.
            PathGeometry picture_path_gemometry = new PathGeometry();
            picture_path_gemometry.Figures.Add(picture_figure);

            // Display the PathGeometry. 
            System.Windows.Shapes.Path picture_path = new System.Windows.Shapes.Path();
            picture_path.Stroke = Brushes.Black;
            picture_path.StrokeThickness = 1;
            picture_path.Data = picture_path_gemometry;
            picture_path.Name = "picture";

            Canvas1.Children.Add(picture_path);

            //フェーズラベル
            PathFigure label_figure = new PathFigure();
            label_figure.StartPoint = new Point(x[(int)phase], y[(int)phase]);
            PolyLineSegment label_line_segment = new PolyLineSegment();
            label_line_segment.Points =
                new PointCollection(mkQua(x[(int)phase], y[(int)phase]));

            label_figure.Segments.Add(label_line_segment);

            /// Create a PathGeometry to contain the figure.
            PathGeometry label_geometry = new PathGeometry();
            label_geometry.Figures.Add(label_figure);

            // Display the PathGeometry. 
            System.Windows.Shapes.Path label_path = new System.Windows.Shapes.Path();
            label_path.Stroke = Brushes.Red;
            label_path.StrokeThickness = 5;
            label_path.Data = label_geometry;
            label_path.Name = "phese_label";
            Canvas1.Children.Add(label_path);

            //ラベルをグラフで
            for (int i = 0; i < label.Length; i++)
            {
                if (label[i] != null)
                {
                    label_figure = new PathFigure();
                    label_figure.StartPoint = new Point(x[i], y[i]);
                    label_line_segment = new PolyLineSegment();
                    label_line_segment.Points =
                        new PointCollection(mkQua(x[i], y[i]));

                    label_figure.Segments.Add(label_line_segment);

                    /// Create a PathGeometry to contain the figure.
                    label_geometry = new PathGeometry();
                    label_geometry.Figures.Add(label_figure);

                    // Display the PathGeometry. 
                    label_path = new System.Windows.Shapes.Path();
                    if (label[i] == "s")
                    {
                        label_path.Stroke = Brushes.Blue;
                    }
                    else if (label[i] == "e")
                    {
                        label_path.Stroke = Brushes.Brown;
                    }
                    else
                    {
                        label_path.Stroke = Brushes.Yellow;
                    }
                    label_path.StrokeThickness = 5;
                    label_path.Data = label_geometry;
                    label_path.Name = "label_" + i + label[i];

                    Canvas1.Children.Add(label_path);
                }
            }

            //ラベルテキストのあっぷっぴ
            string label_text = "";
            for (int i = 0; i < label.Length - 2; i++)
            {
                if (label[i] != null)
                {
                    label_text += label[i] + " -> ";
                }
            }
            label_text += label[label.Length - 1]; //最後のラベルは必ずあるはず
            label_text_block.Text = label_text;
        }

        // [ファイル]-[新規作成]に対するコマンド
        //C#書きたくない!
        /**
         * 速度座標系から、xy座標に
         */
        int ac = 4;
        private int[][] XYVToXY(int[] xv,int[] yv)
        {
            int[] _x = new int[xv.Length];
            int[] _y = new int[yv.Length];
            _x[0] = (int)(Canvas1.ActualWidth / 2);
            _y[0] = (int)(Canvas1.ActualHeight / 2);
            for (int i = 1; i < xv.Length; i++)
            {
                _x[i] = _x[i - 1] + (xv[i] * ac);
                _y[i] = _y[i - 1] + (yv[i] * ac);
            }
            int[][] p = new int[2][];
            p[0] = _x;
            p[1] = _y;
            return p;
        }

        private void getPtsFolderFiles(string opend_file_path)
        {
            string pts_f = System.IO.Path.GetDirectoryName(opend_file_path);
            file_path_buf = Directory.GetFiles(pts_f);
            for (int i = 0; i < file_path_buf.Length; i++)
            {
                if (file_path_buf[i] == opend_file_path)
                {
                    file_path_buf_point = i;
                }
            }
        }

        private string getLabelFileName(string opend_file_path)
        {
            string pts_f = System.IO.Path.GetDirectoryName(opend_file_path);
            string fn = System.IO.Path.GetFileName(opend_file_path).Split('.')[0] + ".lab";
            string p_f = System.IO.Path.GetDirectoryName(pts_f);
            return p_f + "\\label\\" + fn;
        }

        private void UpdateBuffer()
        {
            //初期化
            String fn = file_path_buf[file_path_buf_point];
            is_drawn_canvas = false;
            clearDataGraph();
            clearLabelGraph();
            phase = 0;
            anime_status = 0;

            //ptsファイルの読み込み
            try
            {
                List<int> _x = new List<int>();
                List<int> _y = new List<int>();
                string text = "";
                using (StreamReader sr = new StreamReader(
                        fn, Encoding.GetEncoding("UTF-8")))
                {
                    text = sr.ReadToEnd();
                }
                string[] _t = text.Split('\n');
                for (int i = 1; i < _t.Length - 1; i++) //ヘッドは読み込まない。最後にエンターが入ってる。..ので無視
                {
                    string[] _tt = _t[i].Split(',', '\r');
                    _x.Add(int.Parse(_tt[0]));
                    _y.Add(int.Parse(_tt[1]));
                }
                this.vx = _x.ToArray();
                this.vy = _y.ToArray();
                int[][] p = XYVToXY(this.vx, this.vy);
                this.x = p[0];
                this.y = p[1];
                this.label = new string[this.x.Length];
                this.label[0] = start_label; //スタートラベルは書き換えられない
                this.label[this.label.Length - 1] = "e"; //エンドラベルは書き換わる可能性あり.
            }
            catch (Exception eee)
            {
                Console.Write(eee.Message);
                MessageBox.Show(fn + "が読み込めません");
                return;
            }

            //labファイルの読み込み
            try
            {
                string text = "";
                lab_file = getLabelFileName(fn);
                throw new Exception("ほほほ"); //とりえあえじゅ

                using (StreamReader sr = new StreamReader(
                        lab_file, Encoding.GetEncoding("UTF-8")))
                {
                    text = sr.ReadToEnd();
                }
            }
            catch
            { //なくてもしょうがない

            }

            //無事終えたらcanvasに図を描画
            drawPictureOnCanvas();
            createDataGraph();
            createLabelGraph(); //ラベルグラフを呼ぶまで。。。
            createStartingGraph();
            is_drawn_canvas = true; //描画イベントを始める。
        }

        private void FileNewExecute(object sender, ExecutedRoutedEventArgs e)
        {
            initV();
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "";
            ofd.DefaultExt = "*.pts";
            if (ofd.ShowDialog() == true)
            {
                //ファイル名の取得
                String fn = ofd.FileName;
                getPtsFolderFiles(fn);
                UpdateBuffer();
            }
        }

        // [ファイル]-[保存]に対するコマンド
        private void FileSaveExecute(object sender, ExecutedRoutedEventArgs e)
        {

            //MessageBox.Show("Save");
        }
    }
}
