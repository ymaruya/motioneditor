﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Collections;
using System.Windows.Threading;
using System.Windows;
using System.Collections.ObjectModel;

namespace XamDataChartZoombar
{
    /// <summary>
    /// A dataset representing real-time data
    /// </summary>
    public class TickerBuffer : DependencyObject, INotifyCollectionChanged, IEnumerable
    {
        public int WindowSize
        {
            get
            {
                return _windowSize;
            }
            set
            {
                _windowSize = value;
            }
        }

        private IEnumerable _inputSource;
        private int _position = 0;
        private ObservableCollection<Row> _historicalDataSource = new ObservableCollection<Row>();
        private int _windowSize;
        private DispatcherTimer timer = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(500) };

        public TickerBuffer()
        {
            _windowSize = 1000;
            _inputSource = DataLoader.HistoricalData;
            this.FillWindow();
            timer.Tick += new EventHandler(timer_Tick);
            //timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            AddRecordsFromHistoricalData(1);
        }

        public void StartFeed()
        {
            timer.Start();
        }
        public void StopFeed()
        {
            timer.Stop();
        }

        /// <summary>
        /// Create a buffer using the data supplied, setting a buffer size of 500 items
        /// </summary>
        /// <param name="inputSource">A dataset to be used as input</param>
        public TickerBuffer(IEnumerable inputSource)
        {
            _inputSource = inputSource;
            _windowSize = 500;
        }

        /// <summary>
        /// Creates a buffer using the data supplied, and sets the buffer size to the specified value
        /// </summary>
        /// <param name="inputSource">A dataset to be used as input</param>
        /// <param name="windowSize">A value to be used for buffer size</param>
        public TickerBuffer(IEnumerable inputSource, int windowSize)
        {
            _inputSource = inputSource;
            _windowSize = windowSize;
        }

        public void FillWindow()
        {
            _historicalDataSource.Clear();
            _position = 0;
            for (int i = 0; i < _windowSize; ++i)
            {
                _historicalDataSource.Add((_inputSource as IList)[(_position) % (_inputSource as IList).Count] as Row);
                ++_position;
            }
        }


        /// <summary>
        /// Add a specified number of records containing randomly generated data to dataset
        /// </summary>
        /// <param name="recordCount"></param>
        public void Generate(int recordCount)
        {
            Random r = new Random();
            DateTime startDate = new DateTime(1990, 1, 1, 0, 0, 0);

            for (int i = 0; i < recordCount; ++i)
            {
                double baseValue = r.Next(100);
                double offsetValue = r.Next(5);
                _historicalDataSource.Add(new Row(startDate.AddSeconds(1).ToShortDateString(),baseValue, baseValue + offsetValue, baseValue - offsetValue, baseValue + 2*offsetValue, 0));
            }

            if (CollectionChanged != null)
            {
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
        }

        /// <summary>
        /// Add a specified number of records from historical dataset to dataset
        /// </summary>
        /// <param name="recordCount"></param>
        public void AddRecordsFromHistoricalData(int recordCount)
        {
            //add records from historical data
            for (int i = 0; i < recordCount; ++i)
            {
                _historicalDataSource.Add((_inputSource as IList)[(_position) % (_inputSource as IList).Count] as Row);

                ++_position;
            }

            //remove old records from the beginning of the window according to window size
            while (_historicalDataSource.Count > _windowSize)
                _historicalDataSource.RemoveAt(0);

            if (CollectionChanged != null)
            {
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
        }

        /// <summary>
        /// Add a specified number of records from a historical dataset to this dataset, starting from a specified position
        /// </summary>
        /// <param name="recordCount"></param>
        public void AddRecordsFromHistoricalData(int recordCount, int position)
        {
            _position = position;
            //add records from historical data
            for (int i = 0; i < recordCount; ++i)
            {
                _historicalDataSource.Add((_inputSource as IList)[(_position) % (_inputSource as IList).Count] as Row);

                ++_position;
            }

            //remove old records from the beginning of the window according to window size
            while (_historicalDataSource.Count > _windowSize)
                _historicalDataSource.RemoveAt(0);

            if (CollectionChanged != null)
            {
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
        }



        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return _historicalDataSource.GetEnumerator();
        }

        #endregion

        #region INotifyCollectionChanged Members

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        #endregion
    }

    /// <summary>
    /// Row data schema
    /// </summary>
    public class Row : INotifyPropertyChanged
    {
        public Row(string date, double open, double high, double low, double close, double volume)
        {
            Date = DateTime.ParseExact(date, "M/d/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            Open = open;
            High = high;
            Low = low;
            Close = close;
            Volume = volume;
        }

        public override string ToString()
        {
            return Date.ToString() + ":" + Open.ToString() + ":" + High.ToString() + ":" + Low.ToString() + ":" + Close.ToString() + ":" + Volume.ToString();
        }

        public DateTime Date { get { return column0; } set { column0 = value; RaisePropertyChanged("Column0"); } }
        private DateTime column0;

        public double Open { get { return column1; } set { column1 = value; RaisePropertyChanged("Column1"); } }
        private double column1;

        public double High { get { return column2; } set { column2 = value; RaisePropertyChanged("Column2"); } }
        private double column2;

        public double Low { get { return column3; } set { column3 = value; RaisePropertyChanged("Column3"); } }
        private double column3;

        public double Close { get { return column4; } set { column4 = value; RaisePropertyChanged("Column4"); } }
        private double column4;

        public double Volume { get { return column5; } set { column5 = value; RaisePropertyChanged("Column5"); } }
        private double column5;

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
