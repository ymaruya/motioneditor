﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.PointMarkers;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using System.Globalization;
using System.Windows.Threading;

/**
 * 
 */ 
namespace SimpleTooltip
{
	/// <summary>
	/// Interaction logic for Window1.xaml
	/// </summary>
	public partial class Window1 : Window
	{
		const int N = 100;
		double[] x = new double[N];
		double[] y = new double[N];
        double[] label_x = null;
        double[] label_y = null;

		LineAndMarker<ElementMarkerPointsGraph> data_graph;
        LineAndMarker<ElementMarkerPointsGraph> label_graph;
	

		public Window1()
		{
			InitializeComponent();

            createDataGraph(); 
            createLabelGraph(); //ラベルグラフを呼ぶまで。。。
            createStartingGraph();
		}


        /**
         * アニメーション
         */         
        int phase = 0; //何フェーズか
        int anime_status = 0; //0 => ストップ, 1 => 再生, -1 => 巻き戻し。。。 
        readonly double[] animatedX = new double[2];
        readonly double[] animatedY = new double[2]{20, -20}; //高さは固定
        EnumerableDataSource<double> animatedDataSource = null;
        /// <summary>Programmatically created header</summary>
        Header chartHeader = new Header();
        /// <summary>Text contents of header</summary>
        TextBlock headerContents = new TextBlock();
        /// <summary>Timer to animate data</summary>
        readonly DispatcherTimer timer = new DispatcherTimer();  
        private void AnimatedPlot_Timer(object sender, EventArgs e)
        {
            /***
             * TODO : 図のボタンにも反映を
             */ 
            phase += anime_status;

            for (int i = 0; i < animatedX.Length; i++)
                animatedX[i] = phase; 

            // Here it is - signal that data is updated
            animatedDataSource.RaiseDataChanged();
            headerContents.Text = String.Format(CultureInfo.InvariantCulture, "Phase = {0:N2}", phase);
        }

        private void createStartingGraph()
        {

            for (int i = 0; i < animatedX.Length; i++)
            {
                animatedX[i] = phase;
            }

            animatedDataSource = new EnumerableDataSource<double>(animatedX);
            animatedDataSource.SetXMapping(x => x);

            EnumerableDataSource<double> ySrc = new EnumerableDataSource<double>(animatedY);
            ySrc.SetYMapping(y => y);

            // Adding graph to plotter
            plotter.AddLineGraph(new CompositeDataSource(animatedDataSource, ySrc),
                new Pen(Brushes.Magenta, 3),
                new PenDescription("Sin(x + phase)"));

            timer.Interval = TimeSpan.FromMilliseconds(10);
            timer.Tick += AnimatedPlot_Timer;
            timer.IsEnabled = true;

            // Force evertyhing plotted to be visible
            plotter.FitToView();
        }


        /**
         * 
         */
        private void createDataGraph()
        {
            EnumerableDataSource<double> xDataSource;
            EnumerableDataSource<double> yDataSource;
            IPointDataSource ds;

            // Prepare data in arrays
            for (int i = 0; i < N; i++)
            {
                x[i] = i * 10;
                y[i] = Math.Cos(x[i]);
            }

            // Add data sources:
            yDataSource = new EnumerableDataSource<double>(y);
            yDataSource.SetYMapping(Y => Y);
            yDataSource.AddMapping(ShapeElementPointMarker.ToolTipTextProperty,
                Y => String.Format("Value is {0}", Y));

            xDataSource = new EnumerableDataSource<double>(x);
            xDataSource.SetXMapping(X => X);

            ds = new CompositeDataSource(xDataSource, yDataSource);
            // adding graph to plotter
            data_graph = plotter.AddLineGraph(ds,
                new Pen(Brushes.Purple, 3),
                new CircleElementPointMarker
                {
                    Size = 10,
                    Brush = Brushes.Wheat,
                    Fill = Brushes.Violet
                },
                new PenDescription("Sin"));


            plotter.FitToView();
        }


        /**
         * labelグラフ
         */ 
        private void createLabelGraph()
        {
            EnumerableDataSource<double> xDataSource;
            EnumerableDataSource<double> yDataSource;
            IPointDataSource ds;

            Random r = new Random();
            int n = r.Next(20);
            label_x = new double[n];
            label_y = new double[n];
            // Prepare data in arrays
            for (int i = 0; i < n; i++)
            {
                label_x[i] = i * n;
                label_y[i] = Math.Sin(label_x[i]);
            }

            // Add data sources:
            yDataSource = new EnumerableDataSource<double>(label_y);
            yDataSource.SetYMapping(Y => Y);
            yDataSource.AddMapping(ShapeElementPointMarker.ToolTipTextProperty,
                Y => String.Format("Value is {0}", Y));

            xDataSource = new EnumerableDataSource<double>(label_x);
            xDataSource.SetXMapping(X => X);

            ds = new CompositeDataSource(xDataSource, yDataSource);
            // adding graph to plotter

            label_graph = plotter.AddLineGraph(ds,
                new Pen(Brushes.Black, 3),
                new CircleElementPointMarker
                {
                    Size = 10,
                    Brush = Brushes.Blue,
                    Fill = Brushes.Red
                },
                new PenDescription("Sin"));
            plotter.FitToView();
        }

        private void updateLabelGraph()
        {
            label_graph.LineGraph.Remove();
            label_graph.MarkerGraph.Remove();
            plotter.FitToView();

            createLabelGraph();
        }

        /**
         * めんどくさいので、ラベル付けやその他は全部キーイベントでやる。
         * p -> 1~8 => ラベルをつける
         * r => ラベルを取り除く(現在のx座標から後ろのラベル or 近いラベル どっちがいいかな)
         * c => 再生の位置を設定
         * s => 生成の位置を一番初めに戻す。
         * enter => 再生
         */
        private Boolean is_p_mode = false;
        private Boolean is_simulate_start = false;
        private void KEvent(object sender, System.Windows.Input.KeyEventArgs e)
        {
             TextBlock tb = (TextBlock)cursor1.FindName("horizTextBlock"); //x座標を取得。jquery見たい。
             int _x = (int)double.Parse(tb.Text);
            if (is_p_mode)
            {
                //int _x = int.Parse(tb.Text);
                if (e.Key == Key.D1)
                {
                    updateLabelGraph();
                    MessageBox.Show(tb.Text);
                }
                else if (e.Key == Key.D2)
                {
                    updateLabelGraph();
                }
                else if (e.Key == Key.D3)
                {
                    updateLabelGraph();
                }
                else if (e.Key == Key.D4)
                {
                    updateLabelGraph();
                }
                else if (e.Key == Key.D5)
                {
                    updateLabelGraph();
                }
                else
                {
                    updateLabelGraph();
                    MessageBox.Show("Not exiting such a label");
                }
                is_p_mode = false;
            } 
            else if(is_simulate_start){
                //ここに終了時の処理
                anime_status = 0;
                is_simulate_start = false;
            }
            else
            {
                if (e.Key == Key.P)
                {
                    is_p_mode = true;
                }
                else if (e.Key == Key.R)
                {
                }
                else if (e.Key == Key.C)
                {
                    phase = _x;
                }
                else if (e.Key == Key.S)
                {
                    phase = 0;
                }
                else if (e.Key == Key.Space)
                {
                    anime_status = 1;
                    //ここに開始時の処理
                    is_simulate_start = true;
                }
            }

        }

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			
		}
	}
}
